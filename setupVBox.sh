#!/bin/sh
PWD=$(pwd)
VM=MBRSnake
make mbr
VBoxManage createvm --name $VM --register
VBoxManage modifyvm $VM --firmware bios
VBoxManage modifyvm $VM --ioapic on
VBoxManage modifyvm $VM --memory 4 --vram 32
VBoxManage storagectl $VM --name "IDE Controller" --add ide --controller PIIX4

VBoxManage internalcommands createrawvmdk -filename "snake_mbr.vdmk" -rawdisk "${PWD}/mbr"
VBoxManage storageattach $VM --storagectl "IDE Controller" --port 0 --device 0 --type hdd --medium "snake_mbr.vdmk"
