help:
	@echo "make mbr - to compile MBR"
	@echo "make dosbox - to run mbr in DOSBox"
	@echo "make qemu - to run mbr in Qemu"
	@echo "make bochs - to run mbr in Bochs emulator"
	@echo
	@echo "In order to run snake in VirtualBox you need to execute setupVBox.sh first and then run make vbox"

main.bin: main.fasm
	fasm -d mbr=0 -d debug=0 main.fasm main.bin

mbr: main.fasm
	fasm -d mbr=1 -d debug=0 main.fasm mbr

debug.bin: main.fasm
	fasm -d mbr=0 -d debug=1 main.fasm debug.bin


dump: main.bin
	objdump -w -m i386 -M intel -b binary -M addr16,data16 --adjust-vma=0x0100 -D main.bin | less

dump_mbr: mbr
	objdump -w -m i386 -M intel -b binary -M addr16,data16 --adjust-vma=0x0100 -D mbr | less


test: bochs

debug: debug.bin
	cp debug.bin dos/MAIN.COM
	dosbox -c 'mount c dos' -c 'c:' -c 'TASM\BIN\TD.EXE MAIN.COM' -conf dosbox.conf


dosbox_com: main.bin
	cp main.bin dos/MAIN.COM
	dosbox -c 'mount c dos' -c 'c:' -c 'MAIN.COM' -conf dosbox.conf


dosbox: mbr
	dosbox -c 'boot mbr' -conf dosbox.conf

bochs: mbr
	bochs -q -rc debug.rc

qemu: mbr
	qemu-system-x86_64 -fda mbr -cpu pentium3 -m 1M -vga std -realtime mlock=off -no-user-config

vbox: mbr
	VBoxManage startvm MBRSnake


clean:
	rm -f main.bin mbr debug.bin
